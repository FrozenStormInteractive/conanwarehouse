# Single Sign On with GitHub

> {warning} Conan Warehouse only supports GitHub.com, not GitHub Enterprise.

To enable the GitHub provider, you must use GitHub’s credentials for your instance. To get the credentials (a pair of 
Client ID and Client Secret), you must register an application as an OAuth App on GitHub.

1.  Sign in to [GitHub](https://github.com/)

2.  Navigate to your individual user or organization settings, depending on how you want the application registered. It 
    does not matter if the application is registered as an individual or an organization - that is entirely up to you.
    
    - For individual accounts, select **Developer settings** from the left menu, then select **OAuth Apps**.
    - For organization accounts, directly select **OAuth Apps** from the left menu.

3.  Select **Register an application** (if you don’t have any OAuth App) or **New OAuth App** (if you already have OAuth Apps). 
    ![Register OAuth App](img/github_app_entry.png)

4.  Provide the required details.
    - Application name: This can be anything. Consider something like `<Organization>'s Conan Warehouse` or 
      `<Your Name>'s Conan Warehouse` or something else descriptive.
    - Homepage URL: the URL to your installation. e.g., `https://conan.company.com`
    - Application description: Fill this in if you wish.
    - Authorization callback URL: `http(s)://${YOUR_DOMAIN}`. Please make sure the port is included if your instance is 
      not configured on default port.
      
    ![Register OAuth App](img/github_app_register.png)


5.  Click on **Register application**.

6.  You should now see a pair of **Client ID** and **Client Secret** near the top right of the page.
    Keep this page open as you continue configuration.
    ![GitHub app](img/github_app.png)

7.  On your Warehouse server, open the auth configuration file.

    ```sh
    sudo editor config/auth.php
    ```

    and add the provider configuration:

    ```php
    <?php
  
    return [
   
        ...
    
        'single_sign_on' => [
            'enabled' => true,
            'providers' => ['github']
        ]
    
    ];
    ```

7.  On your Warehouse server, open the services configuration file.

    ```sh
    sudo editor config/services.php
    ```

    and add the provider configuration:

    ```php
    <?php
  
    return [
   
        ...
    
        'github' => [
            'client_id' => 'YOUR_APP_ID',         // Your GitHub Client ID
            'client_secret' => 'YOUR_APP_SECRET', // Your GitHub Client Secret
            'redirect' => 'https://your-warehouse.example.com/connect/github/callback',
        ],
    
    ];
    ```
    
    Change 'YOUR_APP_ID' to the Client ID from the GitHub application page.
    Change 'YOUR_APP_SECRET' to the Client Secret from the GitHub application page.

On the sign in page there should now be a GitHub icon below the regular sign in form.
Click the icon to begin the authentication process. GitHub will ask the user to sign in and authorize your
application.
If everything goes well the user will be returned to your application instance and will be signed in.
