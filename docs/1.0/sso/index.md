# Single Sign On

## Supported Providers

This is a list of the current supported providers. Before proceeding on each provider’s documentation, make 
sure to first read this document as it contains some settings that are common for all providers.

- [Facebook](facebook)
- [Twitter](twitter)
- [Google](google)
- [LinkedIn](linkedin)
- [GitHub](github)
- [GitLab](gitlab)
- [BitBucket](bitbucket)

## Initial Configuration

> {warning} NOTE: SSO is enabled by default. 

To change these settings:

- For installations from source
  
  Open the configuration file:
  
  ```bash
  sudo editor config/auth.php
  ```
  
  and change the following section:

  ```php
  <?php
  
  return [
   
      ...
    
      'single_sign_on' => [
          'enabled' => true,
          'providers' => ['twitter']
      ]
    
  ];
  ```
    
Now we can choose one or more of the Supported Providers listed above to continue the configuration process.

# Enable SSO for an Existing User

Existing users can enable SSO for specific providers after the account is created. For example, if the user originally
signed in, a SSO provider such as Twitter can be enabled. Follow the steps below to enable an SSO provider for an 
existing user.


1. Sign in normally - whether standard sign in, or another SSO provider.
2. Go to profile settings (in the top right corner).
3. Select the “Security” tab.
3. Under “Single Sign On” select the desired SSO provider, such as Twitter.
4. The user will be redirected to the provider. Once the user authorized, they will be redirected back to the 
   application.


## Disabling SSO

Single Sign On is enabled by default. This only has an effect if providers are configured and enabled.

If providers are causing problems even when individually disabled, you can disable the entire SSO subsystem by
modifying the configuration file:

- For installations from source

  ```php
  <?php
    
   return [
    
       ...
      
       'single_sign_on' => [
           'enabled' => false,
           ...
       ]
   ];
   ```
