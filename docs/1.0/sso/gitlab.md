# Single Sign On with GitLab.com

To enable the GitLab.com provider you must register your application with GitLab.com. GitLab.com will generate an 
application ID and secret key for you to use.

1.  Sign in to [GitLab.com](https://gitlab.com/)
2.  On the upper right corner, click on your avatar and go to your **Settings**.
3.  Select **Applications** in the left menu.
4.  Provide the required details for **Add new application**.
    - Name: This can be anything. Consider something like `<Organization>'s Conan Warehouse` or `<Your Name>'s Conan Warehouse` or 
      something else descriptive.
      
    - Redirect URI:
    
      ```
      http://your-warehouse.example.com/connect/gitlab/callback
      ```
      
    ![GitLab appli creation form](img/gitlab_app_create.png)
      
5.  Click on **Save application**.
6.  You should now see a **Application ID** and **Secret**.
    Keep this page open as you continue configuration.
    ![GitLab app](img/gitlab_app.png)


7.  On your Warehouse server, open the auth configuration file.

    ```sh
    sudo editor config/auth.php
    ```

    and add the provider configuration:

    ```php
    <?php
  
    return [
   
        ...
    
        'single_sign_on' => [
            'enabled' => true,
            'providers' => ['gitlab']
        ]
    
    ];
    ```

7.  On your Warehouse server, open the services configuration file.

    ```sh
    sudo editor config/services.php
    ```

    and add the provider configuration:

    ```php
    <?php
  
    return [
   
        ...
    
        'gitlab' => [
            'client_id' => 'YOUR_APP_ID',         // Your GitLab Client ID
            'client_secret' => 'YOUR_APP_SECRET', // Your GitLab Client Secret
            'redirect' => 'https://your-warehouse.example.com/connect/gitlab/callback',
        ],
    
    ];
    ```
    
    Change 'YOUR_APP_ID' to the Application ID from the GitLab.com application page.
    Change 'YOUR_APP_SECRET' to the secret from the GitLab.com application page.

On the sign in page there should now be a GitLab.com icon below the regular sign in form.
Click the icon to begin the authentication process. GitLab.com will ask the user to sign in and authorize your
application.
If everything goes well the user will be returned to your application instance and will be signed in.
