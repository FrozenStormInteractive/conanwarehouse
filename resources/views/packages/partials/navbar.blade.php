<nav class="navbar navbar-dark bg-conan mb-4">
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link @if(Request::routeIs('packages.show')) active @endif" href="{{ route('packages.show', $package) }}">General</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if(Request::routeIs('packages.files')) active @endif" href="{{ route('packages.files', $package) }}">Files</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if(Request::routeIs('packages.versions')) active @endif" href="{{ route('packages.versions', $package) }}">Versions</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if(Request::routeIs('packages.builds')) active @endif" href="{{ route('packages.builds', $package) }}">Builds</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if(Request::routeIs('packages.edit')) active @endif" href="{{ route('packages.edit', $package) }}">Settings</a>
        </li>
    </ul>
</nav>
