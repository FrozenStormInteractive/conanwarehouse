<div class="card col-12 p-0 mb-4">
    <div class="card-body row ml-1 mr-1">
        <div class="col-md-8 pl-0">
            <div class="media">
                <img class="mr-3 avatar" src="{{ $package->avatarUrl }}" alt="{{ $package->name }} package avatar">
                <div class="media-body">
                    <h1 class="mt-0"><b>{{ $package->name }}</b> : {{ $package->user }}</h1>
                    <div>@empty($package->description) @lang("packages.no-description") @else {{ $package->description }} @endempty</div>
                </div>
            </div>
        </div>
        <div class="col-md-4 pl-0">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#set-me-up-modal">Large modal</button>

            @include('partials.SetMeUp-modal')
        </div>
    </div>
</div>
