@extends('layouts.main')

@section('main-content')
    <div class="container-fluid">
        @include('packages.partials.navbar')
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="{{ route('packages.update', $package) }}" method="post" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="card col-12 p-0 mb-4">
                <div class="card-body row ml-1 mr-1">
                    <div class="col-md-9 pl-0">
                        <div class="media">
                            <avatar-image-input class="mr-3 avatar" name="avatar" src="{{ $package->avatarUrl }}"></avatar-image-input>
                            <div class="media-body">
                                <h1 class="mt-0"><b>{{ $package->name }}</b> : {{ $package->user }}</h1>
                                <textarea class="form-control" placeholder="Description" name="description"
                                          rows="4" maxlength="1000">{{ $package->description }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 pl-0">
                        <button type="submit" class="btn btn-success btn-lg btn-block">@lang('packages.update')</button>
                        <button type="button" class="btn btn-danger btn-lg btn-block" data-toggle="modal" data-target="#deleteModal">Delete</button>
                        <a href="{{ route('packages.show', $package) }}" class="btn btn-outline-danger btn-lg btn-block">Cancel</a>
                    </div>
                </div>
            </div>
            <div class="card col-12 p-0 mb-4">
                <div class="card-header" id="about-panel-header">
                    <div class="d-flex bd-highlight mb-0">
                        <h3 class="mb-0">
                            About
                        </h3>
                        <button class="btn ml-auto" type="button" data-toggle="collapse" data-target="#about-panel"
                                aria-expanded="true" aria-controls="about-panel">
                            Expand
                        </button>
                    </div>
                </div>
                <div id="about-panel" class="collapse" aria-labelledby="about-panel-header">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="website-url-input">Website URL</label>
                            <input type="url" class="form-control" id="website-url-input" name="website-url"
                                   value="{{ $package->website_url }}">
                        </div>
                        <div class="form-group">
                            <label for="issuetracker-url-input">Issue Tracker</label>
                            <input type="url" class="form-control" id="issuetracker-url-input" name="issuetracker-url"
                                   value="{{ $package->issue_tracker_url }}">
                        </div>
                        <div class="form-group">
                            <label for="vcs-url-input">VCS</label>
                            <input type="url" class="form-control" id="vcs-url-input" name="vcs-url"
                                   value="{{ $package->vcs_url }}">
                        </div>
                        <div class="form-group">
                            <label for="licenses-url-input">Licenses</label>
                            <input type="text" class="form-control" id="licenses-url-input" name="licenses"
                                   value="MIT">
                        </div>
                    </div>
                </div>
            </div>
            <div class="card col-12 p-0">
                <div class="card-header" id="visibility-panel-header">
                    <div class="d-flex bd-highlight mb-0">
                        <h3 class="mb-0">
                            Permissions
                        </h3>
                        <button class="btn ml-auto" type="button" data-toggle="collapse" data-target="#visibility-panel"
                                aria-expanded="true" aria-controls="visibility-panel">
                            Expand
                        </button>
                    </div>
                </div>
                <div id="visibility-panel" class="collapse" aria-labelledby="visibility-panel-header">
                    <div class="card-body">
                        <fieldset>
                            <h4>Visibility</h4>
                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="visibility" id="public-visibility"
                                           value="{{ \App\Enums\VisibilityType::PUBLIC }}" @if($package->visibility == \App\Enums\VisibilityType::PUBLIC) checked @endif>
                                    <label class="form-check-label" for="public-visibility">Public</label>
                                </div>
                                <small id="passwordHelpBlock" class="form-text text-muted">
                                    The package can be accessed by anyone, regardless of authentication. The repository must be public.
                                </small>
                            </div>
                            <div class="form-group mb-0">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="visibility" id="private-visibility"
                                           value="{{ \App\Enums\VisibilityType::PRIVATE }}" @if($package->visibility == \App\Enums\VisibilityType::PRIVATE) checked @endif>
                                    <label class="form-check-label" for="private-visibility">Private</label>
                                </div>
                                <small id="passwordHelpBlock" class="form-text text-muted">
                                    The package is accessible only by members of the repository. Access must be granted explicitly to each user.
                                </small>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </form>
        <deletion-modal id="deleteModal" name="{{ $package->name }}" action="{{ route('packages.destroy', $package) }}"></deletion-modal>
    </div>
@endsection
