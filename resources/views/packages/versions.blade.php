@extends('layouts.main')

@section('main-content')
    <div class="container-fluid">
        @include('packages.partials.navbar')
        @include('packages.partials.header')
        <div class="card col-12 p-0 mb-4">
            <h3 class="card-header">Versions</h3>
            <div class="card-body">
                <table class="table mb-0">
                    <thead>
                        <tr>
                            <th scope="col">Version</th>
                            <th scope="col">References</th>
                            <th scope="col">Builds</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($versions as $version)
                            <tr>
                                <td class="w-25">
                                    <a href=""><b>{{ $version->version_string }}</b> : {{ $version->channel }}</a>
                                </td>
                                <td class="w-25">
                                    {{ $version->reference }}
                                </td>
                                <td>
                                    @if($version->hasWindowsBuild)
                                        <i class="fab fa-windows fa-lg text-windows" data-toggle="tooltip" data-placement="top" title="Windows"></i>
                                    @endif
                                    @if($version->hasAppleBuild)
                                        <i class="fab fa-apple fa-lg text-apple" data-toggle="tooltip" data-placement="top" title="Apple"></i>
                                    @endif
                                    @if($version->hasLinuxBuild)
                                        <i class="fab fa-linux fa-lg text-tux" data-toggle="tooltip" data-placement="top" title="Linux"></i>
                                    @endif
                                    @if($version->hasFreeBSDBuild)
                                        <i class="fab fa-freebsd fa-lg text-windows" data-toggle="tooltip" data-placement="top" title="FreeBSD"></i>
                                    @endif
                                    @if($version->hasAndroidBuild)
                                        <i class="fab fa-android fa-lg text-windows" data-toggle="tooltip" data-placement="top" title="Android"></i>
                                    @endif
                                    @if($version->hasArduinoBuild)
                                        <i class="fabc fa-arduino fa-lg text-arduino" data-toggle="tooltip" data-placement="top" title="Arduino"></i>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $versions->links() }}
            </div>
        </div>
    </div>
@endsection
