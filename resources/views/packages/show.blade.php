@extends('layouts.main')

@section('main-content')
    <div class="container-fluid">
        @include('packages.partials.navbar')
        @include('packages.partials.header')
        <div class="card col-12 p-0 mb-4">
            <h3 class="card-header">About</h3>
            <div class="card-body">
                <table class="table mb-0">
                    <tbody>
                        <tr>
                            <th scope="row" class="w-25">Website</th>
                            <td>
                                @empty($package->website_url)
                                    <span class="disabled">None</span>
                                @else
                                    <a href="{{ $package->website_url }}">{{ $package->website_url }}</a>
                                @endempty
                            </td>
                        </tr>
                        <tr>
                            <th scope="row" class="w-25">Issue Tracker</th>
                            <td>
                                @empty($package->issue_tracker_url)
                                    <span class="disabled">None</span>
                                @else
                                    <a href="{{ $package->issue_tracker_url }}">{{ $package->issue_tracker_url }}</a>
                                @endempty
                            </td>
                        </tr>
                        <tr>
                            <th scope="row" class="w-25">VCS</th>
                            <td>
                                @empty($package->vcs_url)
                                    <span class="disabled">None</span>
                                @else
                                    <a href="{{ $package->vcs_url }}">{{ $package->vcs_url }}</a>
                                @endempty
                            </td>
                        </tr>
                        <tr>
                            <th scope="row" class="w-25">Licenses</th>
                            <td>MIT</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
