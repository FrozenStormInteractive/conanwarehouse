@extends('layouts.app')

@section('content')
    <div class="sidebar-wrapper">
        <!-- Sidebar  -->
        <nav class="sidebar">
            <ul class="list-unstyled components">
                <li>
                    <a href="#">
                        <i class="fas fa-home"></i> Home
                    </a>
                    <a href="{{ route('repos.index') }}">
                        <i class="fas fa-database"></i>
                        Repositories
                    </a>
                    <a href="#">
                        <i class="fas fa-box"></i>
                        Packages
                    </a>
                    <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <i class="fas fa-copy"></i>
                        Pages
                    </a>
                    <ul class="collapse list-unstyled" id="pageSubmenu">
                        <li>
                            <a href="#">Page 1</a>
                        </li>
                        <li>
                            <a href="#">Page 2</a>
                        </li>
                        <li>
                            <a href="#">Page 3</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>

        <div class="content">
            @yield('main-content')
        </div>


    </div>
@endsection


