@extends('layouts.app')

@section('content')
    <div class="sidebar-wrapper">
        <!-- Sidebar  -->
        <nav class="sidebar">
            <ul class="list-unstyled components">
                <li>
                    <a href="{{ route('settings.profile') }}">
                        <i class="fas fa-user"></i> Profile
                    </a>
                    <a href="{{ route('settings.password') }}">
                        <i class="fas fa-key"></i> Password
                    </a>
                    <a href="{{ route('settings.security') }}">
                        <i class="fas fa-lock"></i> Security
                    </a>
                    <a href="#developer-submenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <i class="fas fa-laptop"></i> Developer settings
                    </a>
                    <ul class="collapse list-unstyled" id="developer-submenu">
                        <li>
                            <a href="{{ route('settings.access-tokens') }}">Personal Access Tokens</a>
                        </li>
                        <li>
                            <a href="{{ route('settings.applications') }}">Applications</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
        <div class="content">
            @yield('main-content')
        </div>
    </div>
@endsection


