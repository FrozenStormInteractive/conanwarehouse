@extends('layouts.main')

@section('main-content')
    <div class="container-fluid">
        @include('repositories.partials.navbar')
        @include('repositories.partials.header')
        {{ $packages->links() }}
        <div class="row">
            @foreach($packages as $package)
                <div class="col-xs-12 col-lg-6 col-xl-4">
                    <div class="card rounded-0 package-box">
                        <div class="card-body d-flex">
                            <div>
                                <a href="{{ route('packages.show', $package) }}" class="d-block h5"><b>{{ $package->name }}</b> : {{ $package->user }}</a>
                                <p class="mb-0">
                                    {{ $package->description }}
                                </p>
                            </div>

                            <img class="mr-3 avatar ml-auto" src="{{ $package->avatar_url }}" alt="{{ $package->reference }} package avatar">

                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        {{ $packages->links() }}
    </div>
@endsection
