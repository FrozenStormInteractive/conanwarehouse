<div class="card col-12 p-0 mb-4">
    <div class="card-body row ml-1 mr-1">
        <div class="col-md-8 pl-0">
            <div class="media">
                <img class="mr-3 avatar" src="{{ $repo->avatar_url }}" alt="{{ $repo->name }} package avatar">
                <div class="media-body">
                    <div>
                        <h1 class="mt-0 d-inline"><b>{{ $repo->name }}</b></h1>
                        @include('repositories.partials.visibility-icon')
                    </div>
                    <div>@empty($repo->description) @lang("repos.no-description") @else {{ $repo->description }} @endempty</div>
                </div>
            </div>
        </div>
        <div class="col-md-4 pl-0">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#set-me-up-modal">Large modal</button>

            @include('partials.SetMeUp-modal')
        </div>
    </div>
</div>
