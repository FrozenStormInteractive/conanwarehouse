@extends('layouts.main')

@section('main-content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3">
                <h4>New repository</h4>
                <p>
                    A project is where you house your packages, plan your work (issues), and publish your
                    documentation (wiki), among other things.
                </p>
                <p>
                    All features are enabled for blank projects, from templates, or when importing, but you can disable
                    them afterward in the project settings.
                </p>
            </div>
            <div class="col-lg-9">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{ route('repos.store') }}" method="post" class="card">
                    @csrf
                    <div class="card-header">Header</div>
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-xl-6 mb-xl-0">
                                <label for="name-input">Name</label>
                                <input type="text" class="form-control" id="name-input" placeholder="My repository"
                                       name="name" required>
                            </div>
                            <div class="form-group col-xl-6 mb-0">
                                <label for="path-input">Path</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">{{ route('repos.show.key', '') }}/</span>
                                    </div>
                                    <input type="text" class="form-control" id="path-input" name="path" placeholder="my-repository"
                                           title="Please choose a group path with no special characters."
                                           pattern="[a-zA-Z0-9_\.][a-zA-Z0-9_\-]*" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="desc-input">Description (optional)</label>
                            <textarea name="description" class="form-control" rows="3" id="desc-input" placeholder="Description"></textarea>
                        </div>
                        <label for="a">Visibility</label>
                        <div class="form-group">
                            <div class="form-check mb-2">
                                <input class="form-check-input" type="radio" name="visibility" id="visibility-public" value="public" checked>
                                <label for="visibility-public" class="form-check-label">
                                    <span class="d-block">Public</span>
                                    <span class="d-block form-text text-muted mt-0">The repository can be accessed without any authentication.</span>
                                </label>
                            </div>
                            <div class="form-check mb-2">
                                <input class="form-check-input" type="radio" name="visibility" id="visibility-private" value="private">
                                <label for="visibility-private" class="form-check-label">
                                    <span class="d-block">Private</span>
                                    <span class="d-block form-text text-muted mt-0">Access must be granted explicitly to each user.</span>
                                </label>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success">Create</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
