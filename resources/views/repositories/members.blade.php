@extends('layouts.main')

@section('main-content')
    <div class="container-fluid">
        @include('repositories.partials.navbar')
        @include('repositories.partials.header')

        <div class="card col-12 p-0 mb-4">
            <h3 class="card-header">Invite members</h3>
            <div class="card-body">
                <form action="{{ route('repos.members.add', $repo) }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="members-select">Select members to invite</label>
                        <member-search-input id="members-select"></member-search-input>
                    </div>
                    <div class="form-group">
                        <label for="role-select">Choose a role permission</label>
                        <select id="role-select" class="form-control" name="permission_level">
                            @foreach(\App\Enums\PermissionType::toSelectArray() as $key => $str)
                                <option value="{{ $key }}">{{ $str }}</option>
                            @endforeach
                        </select>
                        <small class="form-text text-muted">Read more about role permissions</small>
                    </div>
                    <div class="form-group">
                        <label for="expiration-date-input">Access expiration date</label>
                        <input type="date" class="form-control" id="expiration-date-input" placeholder="Expiration date" name="expires_at">
                    </div>
                    <button type="submit" class="btn btn-success">Add to repository</button>
                </form>
            </div>
        </div>
        <div class="card col-12 p-0 mb-4">
            <div class="card-header d-flex">
                <h3>Members of {{ $repo->name }} <span class="badge badge-secondary">{{ $members->total() + 1 }}</span></h3>
                <form class="form-inline ml-auto">
                    <div class="input-group mr-2">
                        <input type="text" class="form-control" placeholder="Find existing members by name" name="search" value="{{ request('search') }}">
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="submit"><i class="fas fa-search"></i></button>
                        </div>
                    </div>

                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            Sort
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                            <h6 class="dropdown-header">Sort by</h6>
                            <a class="dropdown-item" href="{{ url()->current().'?'. http_build_query(['sort' => sortable_parameter('name', \App\Enums\OrderType::ASC) ] + request()->query()) }}">Name, ascending</a>
                            <a class="dropdown-item" href="{{ url()->current().'?'. http_build_query(['sort' => sortable_parameter('name', \App\Enums\OrderType::DESC) ] + request()->query()) }}">Name, descending</a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="card-body">
                <table class="table mb-0">
                    <tr>
                        <td class="media">
                            <img class="mr-3 avatar-sm" src="{{ $repo->owner->avatar_url }}" alt="Generic placeholder image">
                            <div class="media-body d-flex">
                                <div>
                                    <a href="{{ route('users.show', $repo->owner) }}">{{ $repo->owner->name }}</a>
                                </div>
                                <div class="ml-auto text-right">
                                    <div>Owner</div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @foreach($members as $member)
                        <tr>
                            <td class="media">
                                <img class="mr-3 avatar-sm" src="{{ $member->avatar_url }}" alt="Generic placeholder image">
                                <div class="media-body d-flex">
                                    <div>
                                        <a href="{{ route('users.show', $member) }}">{{ $member->name }}</a>
                                    </div>
                                    <div class="ml-auto text-right">
                                        <div>{{ \App\Enums\PermissionType::getDescription($member->pivot->permission_type) }}</div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </table>
                {{ $members->links() }}
            </div>
        </div>
    </div>
@endsection
