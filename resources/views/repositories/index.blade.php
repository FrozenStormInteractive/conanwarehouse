@extends('layouts.main')

@section('main-content')
    <h1>Repositories</h1>
    <nav class="navbar navbar-dark bg-conan">
        <ul class="nav nav-pills">
            <li class="nav-item active">
                <a class="nav-link" href="#">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Features</a>
            </li>
        </ul>
        <form class="form-inline">
            <input class="form-control mr-sm-2" type="search" placeholder="Filter by name..." aria-label="Search">
            <select name="" id="" class="form-control">
                <option value="">aa</option>
            </select>
            <a href="{{ route('repos.create') }}" class="btn btn-success">Create repository</a>
        </form>
    </nav>
    <table class="table">
        @foreach($repos as $repo)
            <tr>
                <td class="media">
                    <img class="mr-3 avatar-sm" src="{{ $repo->avatar_url }}" alt="Generic placeholder image">
                    <div class="media-body d-flex">
                        <div>
                            <h4 class="mb-0"><a href="{{ route('repos.show.key', $repo->path) }}">{{ $repo->name }}</a></h4>
                            {{ $repo->description }}
                        </div>
                        <div class="ml-auto text-right">
                            <div>
                                @include('repositories.partials.visibility-icon')
                            </div>
                            <div>Updated {{ $repo->updated_at->diffForHumans(\Carbon\Carbon::now()) }}</div>
                        </div>
                    </div>
                </td>
            </tr>
        @endforeach
    </table>
    {{ $repos->links() }}
@endsection
