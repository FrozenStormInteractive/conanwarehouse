@extends('layouts.main')

@section('main-content')
    <div class="container-fluid">
        @include('repositories.partials.navbar')
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="{{ route('repos.update', $repo) }}" method="post" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="card col-12 p-0 mb-4">
                <div class="card-body row ml-1 mr-1">
                    <div class="col-md-9 pl-0">
                        <div class="media">
                            <avatar-image-input class="mr-3 avatar" name="avatar" src="{{ $repo->avatarUrl }}"></avatar-image-input>
                            <div class="media-body">
                                <input type="text" class="form-control mb-2" name="name" value="{{ $repo->name }}">
                                <textarea class="form-control" placeholder="Description" name="description"
                                          rows="4" maxlength="1000">{{ $repo->description }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 pl-0">
                        <button type="submit" class="btn btn-success btn-lg btn-block">@lang('packages.update')</button>
                        <button type="button" class="btn btn-danger btn-lg btn-block" data-toggle="modal" data-target="#deleteModal">Delete</button>
                        <a href="{{ route('repos.show', $repo) }}" class="btn btn-outline-danger btn-lg btn-block">Cancel</a>
                    </div>
                </div>
            </div>
            <div class="card col-12 p-0 mb-4">
                <div class="card-header" id="permissions-panel-header">
                    <div class="d-flex bd-highlight mb-0">
                        <h3 class="mb-0">
                            Permissions
                        </h3>
                        <button class="btn ml-auto" type="button" data-toggle="collapse" data-target="#permissions-panel"
                                aria-expanded="true" aria-controls="permissions-panel">
                            Expand
                        </button>
                    </div>
                </div>
                <div id="permissions-panel" class="collapse" aria-labelledby="permissions-panel-header">
                    <div class="card-body">
                        <fieldset>
                            <h4>Visibility</h4>
                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="visibility" id="public-visibility"
                                           value="{{ \App\Enums\VisibilityType::PUBLIC }}" @if($repo->visibility == \App\Enums\VisibilityType::PUBLIC) checked @endif>
                                    <label class="form-check-label" for="public-visibility">Public</label>
                                </div>
                                <small id="passwordHelpBlock" class="form-text text-muted">
                                    The repository can be accessed by anyone, regardless of authentication.
                                </small>
                            </div>
                            <div class="form-group mb-0">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="visibility" id="private-visibility"
                                           value="{{ \App\Enums\VisibilityType::PRIVATE }}" @if($repo->visibility == \App\Enums\VisibilityType::PRIVATE) checked @endif>
                                    <label class="form-check-label" for="private-visibility">Private</label>
                                </div>
                                <small id="passwordHelpBlock" class="form-text text-muted">
                                    The repository is accessible only by members. Access must be granted explicitly to each user.
                                </small>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </form>
        <deletion-modal id="deleteModal" name="{{ $repo->path }}" action="{{ route('repos.destroy', $repo) }}"></deletion-modal>
    </div>
@endsection
