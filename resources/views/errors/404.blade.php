@extends('errors.layout')

@section('title', "The page you're looking for could not be found (404)")

@section('content')
    <h3>The page could not be found or you don't have permission to view it.</h3>
    <hr />
    <p>The resource that you are attempting to access does not exist or you don't have the necessary permissions to view it.</p>
    <p>Make sure the address is correct and that the page hasn't moved.</p>
    <p>Please contact your administrator if you think this is a mistake.</p>
@endsection
