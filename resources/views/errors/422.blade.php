@extends('errors.layout')

@section('title', "The change you requested was rejected (422)")

@section('content')
    <h3>The change you requested was rejected.</h3>
    <hr />
    <p>Make sure you have access to the thing you tried to change.</p>
    <p>Please contact your administrator if you think this is a mistake.</p>
@endsection
