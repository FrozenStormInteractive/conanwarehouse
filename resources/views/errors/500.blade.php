@extends('errors.layout')

@section('title', "Something went wrong (500)")

@section('content')
    <h3>Whoops, something went wrong on our end.</h3>
    <hr />
    <p>Try refreshing the page, or going back and attempting the action again.</p>
    <p>Please contact your administrator if this problem persists.</p>
@endsection
