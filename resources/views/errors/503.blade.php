@extends('errors.layout')

@section('title', "Warehouse is not responding (502)")

@section('content')
    <h3>Whoops, Warehouse is currently unavailable.</h3>
    <hr />
    <p>Try refreshing the page, or going back and attempting the action again.</p>
    <p>Please contact your administrator if this problem persists.</p>
@endsection
