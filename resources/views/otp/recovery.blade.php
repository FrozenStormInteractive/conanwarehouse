@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center auth-panel">
            <div class="col-lg-5 col-md-8">
                <div class="card">
                    <div class="card-header">@lang('Two-factor recovery')</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('2fa.recovery') }}">
                            @csrf
                            <div class="form-group">
                                <label for="recovery-code-input" class="text-md-right">@lang('Recovery code')</label>
                                <input id="recovery-code-input" type="text" class="form-control{{ $errors->has('recovery_code') ? ' is-invalid' : '' }}" name="recovery_code" required autofocus>

                                @if ($errors->has('recovery_code'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('recovery_code') }}</strong>
                                </span>
                                @endif
                                <small class="form-text text-muted">
                                    You can enter one of your recovery codes in case you lost access to your mobile device.
                                </small>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">
                                    Verify code
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
