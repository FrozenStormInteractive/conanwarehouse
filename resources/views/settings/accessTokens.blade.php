@extends('layouts.settings')

@section('main-content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4">
                <h4>Personal Access Tokens</h4>
                <p>
                    You can generate a personal access token for each application you use that needs access to the API.
                </p>
                <p>
                    You can also use personal access tokens to authenticate against Git over HTTP. They are the only
                    accepted password when you have Two-Factor Authentication (2FA) enabled.
                </p>
            </div>
            <div class="col-lg-8">
                <personal-access-tokens></personal-access-tokens>
            </div>
        </div>
    </div>
@endsection
