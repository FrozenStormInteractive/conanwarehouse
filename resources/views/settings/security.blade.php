@extends('layouts.settings')

@section('main-content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4">
                <h4>Two-Factor Authentication</h4>
                <p>
                    Increase your account's security by enabling Two-Factor Authentication (2FA).
                </p>
            </div>
            <div class="col-lg-8">
                <p>
                    Status: @if($user->otpEnabled) <i class="fas fa-check fa-lg text-success"></i> enabled @else <i class="fas fa-times fa-lg text-danger"></i> disabled @endif
                </p>
                @if($user->otpEnabled)
                    <p>
                        You've already enabled two-factor authentication using one time password authenticator. In order
                        to register a different device, you must first disable two-factor authentication.
                    </p>
                    <p>
                        If you lose your recovery codes you can generate new ones, invalidating all previous codes.
                    </p>
                    <a href="{{ route('settings.otp.disable') }}" class="btn btn-danger"
                       onclick="event.preventDefault(); document.getElementById('disable-2fa-form').submit();">
                        @lang('Disable two-factor authentication')
                    </a>
                    <form id="disable-2fa-form" action="{{ route('settings.otp.disable') }}" method="POST" class="d-none">
                        @csrf
                    </form>

                    <a href="{{ route('settings.otp.codes') }}" class="btn btn-secondary"
                       onclick="event.preventDefault(); document.getElementById('reset-codes-form').submit();">
                        @lang('Regenerate recovery codes')
                    </a>
                    <form id="reset-codes-form" action="{{ route('settings.otp.codes') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                @else
                    <a href="{{ route('settings.otp.setup') }}" class="btn btn-success">Enable two-factor authentication</a>
                @endif
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-lg-4">
                <h4>Universal Two-Factor (U2F) Devices</h4>
                <p>
                    Use a hardware device to add the second factor of authentication.
                </p>
                <p>
                    As U2F devices are only supported by a few browsers, we encourage that you
                    <strong>set up a two-factor authentication app</strong> before a U2F device. That way you'll always
                    be able to log in - even when you're using an unsupported browser.
                </p>
            </div>
            <div class="col-lg-8">
                <webauthn-devices></webauthn-devices>
            </div>
        </div>
        @if(config('auth.single_sign_on.enabled') and count(config('auth.single_sign_on.providers')) > 0)
        <hr>
        <div class="row">
            <div class="col-lg-4">
                <h4>Connected accounts</h4>
                <p>
                    Activate signin with one of the following services
                </p>
            </div>
            <div class="col-lg-8">
                <p>
                    Click on icon to activate signin with one of the following services
                </p>
                @foreach(config('auth.single_sign_on.providers') as $provider)
                <div class="d-inline-block mb-2 mr-1">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fab fa-{{ $provider }} fa-2x"></i></span>
                        </div>
                        <div class="input-group-append">
                            @if($user->hasSsoIdentity($provider))
                                <form id="account-disconnect-form-{{ $provider }}" action="{{ route('settings.accounts.disconnect', $provider) }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                                <a class="btn btn-outline-danger" href="{{ route('settings.accounts.disconnect', $provider) }}"
                                   onclick="event.preventDefault(); document.getElementById('account-disconnect-form-{{ $provider }}').submit();">
                                    Disconnect
                                </a>
                                @else
                                <a class="btn btn-outline-success" href="{{ route('sso.redirect', $provider) }}">Connect</a>
                            @endif
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        @endif
    </div>
@endsection
