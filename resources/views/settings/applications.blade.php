@extends('layouts.settings')

@section('main-content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4">
                <h4>Applications</h4>
                <p>
                    Manage applications that can use Conan Warehouse API, and applications that you've authorized to use your account.
                </p>
            </div>
            <div class="col-lg-8">
                <passport-clients></passport-clients>
                <hr>
                <authorized-clients></authorized-clients>
            </div>
        </div>
    </div>
@endsection
