@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center auth-panel">
        <div class="col-lg-8 col-md-10">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <div class="row">
                        <form method="POST" class="col-xs-12 col-md" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                            @csrf

                            <div class="form-group ">
                                <label for="email" class="text-md-right">{{ __('E-Mail Address') }}</label>
                                <input id="email" name="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" required autofocus placeholder="Enter email">

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="password" class="text-md-right">{{ __('Password') }}</label>
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group mb-0 d-flex flex-row align-items-center">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                                <button type="submit" class="btn btn-primary ml-auto">
                                    {{ __('Log In') }}
                                </button>
                            </div>
                        </form>
                        @if(config('auth.single_sign_on.enabled') and count(config('auth.single_sign_on.providers')) > 0)
                            <!-- divider -->
                            <div class="border-right d-xs-none"></div>
                            <div class="col-xs-12 col-md mt-4 mt-xl-0">
                                @foreach(config('auth.single_sign_on.providers') as $provider)
                                    <a class="btn btn-outline-secondary btn-block" href="{{ route('sso.redirect', $provider) }}"><i class="fab fa-{{ $provider }} fa-lg"></i> Connect with {{ $provider }}</a>
                                @endforeach
                            </div>
                        @endif
                    </div>

                </div>
            </div>
            <p class="text-center mt-3">
                <a href="{{ route('password.request') }}">
                    {{ __('Forgot Your Password?') }}
                </a>
            </p>
            <p class="text-center mt-3">
                {{ __("Don't have an account?") }} <a href="{{ route('register') }}">{{ __('Register') }}</a>
            </p>
        </div>
    </div>
</div>
@endsection
