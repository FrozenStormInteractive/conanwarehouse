
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('member-search-input', require('./components/MemberSearchInput'));

Vue.component('avatar-image-input', require('./components/AvatarImageInput.vue'));
Vue.component('deletion-modal', require('./components/DeletionModal.vue'));
Vue.component('file-tree-view', require('./components/TreeView/TreeView.vue'));

Vue.component('personal-access-tokens', require('./components/passport/PersonalAccessTokens.vue'));
Vue.component('passport-clients', require('./components/passport/Clients.vue'));

Vue.component('authorized-clients', require('./components/passport/AuthorizedClients.vue'));

const app = new Vue({
    el: '#app'
});
