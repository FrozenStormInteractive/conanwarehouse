<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes(['verify' => true]);
Route::middleware(['auth'])->group(function () {
    Route::get('/password/expired/', 'Auth\ExpiredPasswordController@showResetForm')->name('password.expired.form');
    Route::post('/password/expired/', 'Auth\ExpiredPasswordController@reset')->name('password.expired.reset');
});

Route::get('/', 'HomeController@index')->name('home');
Route::get('/search', 'SearchController@search')->name('search');

/*
 * Auth
 */
Route::resource('users', 'UserController')->only(['show']);

Route::get('/two-factor', 'Auth\LoginController@show2faForm')->name('2fa.validate.form');
Route::post('/two-factor', 'Auth\LoginController@validate2fa')->name('2fa.validate');
Route::get('/two-factor/recovery', 'Auth\LoginController@showRecoveryForm')->name('2fa.recovery.form');
Route::post('/two-factor/recovery', 'Auth\LoginController@recover')->name('2fa.recovery');

/*
 * SSO
 */
if (config('auth.single_sign_on.enabled')) {
    Route::get('/connect/{provider}', 'Auth\LoginController@redirectToProvider')->name('sso.redirect');
    Route::get('/connect/{provider}/callback', 'Auth\LoginController@handleProviderCallback')->name('sso.callback');
}

/*
 * Media
 */

Route::get('/medias/{media}/', 'MediaController@show')->name('medias.show');

/*
 * Settings
 */
Route::prefix('settings')->group(function () {
    Route::get('/profile', 'SettingsController@profile')->name('settings.profile');
    Route::patch('/profile', 'SettingsController@updateProfile')->name('settings.profile.update');
    Route::get('/password', 'SettingsController@password')->name('settings.password');
    Route::patch('/password', 'SettingsController@updatePassword')->name('settings.password.update');
    Route::get('/security', 'SettingsController@security')->name('settings.security');
    Route::get('/personal_access_tokens', 'SettingsController@accessTokens')->name('settings.access-tokens');
    Route::get('/applications', 'SettingsController@applications')->name('settings.applications');

    if (config('auth.single_sign_on.enabled')) {
        Route::post('/accounts/{provider}/disconnect', 'SettingsController@disconnectAccount')
            ->name('settings.accounts.disconnect');
    }

    Route::get('/security/otp/setup', 'SettingsController@setupOtp')->name('settings.otp.setup');
    Route::post('/security/otp/setup', 'SettingsController@validateOtpSetup')->name('settings.otp.validate');
    Route::post('/security/otp/codes', 'SettingsController@regenerateOtpRecoveryCodes')->name('settings.otp.codes');
    Route::post('/security/otp/disable', 'SettingsController@disableOtp')->name('settings.otp.disable');
});

/*
 * Repositories & packages
 */

Route::resource('repos', 'RepositoryController');
Route::get('/r/{key}', 'RepositoryController@showWithKey')->name('repos.show.key');
Route::get('/repos/{repo}/members', 'RepositoryController@showMembers')->name('repos.members.show');
Route::post('/repos/{repo}/members', 'RepositoryController@addMembers')->name('repos.members.add');


Route::resource('packages', 'PackageController')->except(['create', 'store']);
Route::get('/packages/{package}/files/', 'PackageController@browseFile')->name('packages.files');
Route::get('/packages/{package}/versions/', 'PackageController@showVersions')->name('packages.versions');
Route::get('/packages/{package}/builds/', 'PackageController@showBuilds')->name('packages.builds');
Route::get('/packages/{package}/builds/{build}', 'PackageController@showBuild')->name('packages.build');
