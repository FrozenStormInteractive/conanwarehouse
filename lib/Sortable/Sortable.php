<?php

namespace Lib\Sortable;

use App\Enums\OrderType;

trait Sortable
{
    /**
     * Use App\Enums\OrderType
     *
     * @var string
     */
    protected $defaultDirection = OrderType::ASC;

    public function scopeSort($query, $column = null, $direction = null)
    {
        if ($column) {
            if (!$direction) {
                $direction = OrderType::ASC;
            }
        } elseif (request()->has('sort')) {
            list($column, $direction) = $this->parseParameter(request()->input('sort'));
        } else {
            return $query;
        }

        return $this->buildQuery($query, $column, $direction);
    }

    /**
     * @param array $parameters
     *
     * @return array
     */
    private function parseParameter(string $parameter)
    {
        if (empty($parameter)) {
            return [null, null];
        }

        $pos = strrpos($parameter, '_');

        if ($pos === false) {
            return [$parameter, $this->defaultDirection];
        }

        $column = substr($parameter, 0, $pos);
        $direction = substr($parameter, $pos + 1);

        if (!OrderType::hasValue($direction)) {
            $direction = $this->defaultDirection;
        }
        return [$column, $direction];
    }

    private function isSortableColumn($column)
    {
        return isset($this->sortable) && in_array($column, $this->sortable);
    }

    /**
     * @param \Illuminate\Database\Query\Builder $query
     * @param $column
     * @param $direction
     * @return \Illuminate\Database\Query\Builder
     */
    private function buildQuery($query, $column, $direction)
    {
        if (is_null($column)) {
            return $query;
        }
        if (method_exists($this, camel_case($column).'Sortable')) {
            return call_user_func_array([$this, camel_case($column).'Sortable'], [$query, $direction]);
        }
        if (isset($this->sortableAs) &&in_array($column, $this->sortableAs)) {
            $query = $query->orderBy($column, $direction);
        } elseif ($this->isSortableColumn($column)) {
            $column = $this->getTable().'.'.$column;
            $query  = $query->orderBy($column, $direction);
        }
        return $query;
    }
}
