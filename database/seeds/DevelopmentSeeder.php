<?php

use Illuminate\Database\Seeder;

class DevelopmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            \Seed\Development\AdminSeeder::class,
            \Seed\Development\RepositorySeeder::class,
        ]);
    }
}
