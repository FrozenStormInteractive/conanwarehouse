<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageBuildsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_builds', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('conan_id');
            $table->string('arch')->default('');
            $table->string('os')->default('');
            $table->string('compiler')->default('');
            $table->string('compiler_version')->default('');
            $table->json('options')->default('');

            $table->unsignedInteger('package_version_id');

            $table->foreign('package_version_id')->references('id')->on('package_versions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('package_builds');
    }
}
