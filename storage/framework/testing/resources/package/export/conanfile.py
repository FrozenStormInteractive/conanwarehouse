#!/usr/bin/env python
# -*- coding: utf-8 -*-

from conans import ConanFile, tools


class ArgsConan(ConanFile):
    name = "args"
    version = "6.2.0"
    description = "A simple header-only C++ argument parser library."
    url = "https://gitlab.com/ArsenStudio/ArsenEngine/dependencies/conan-{0}".format(name)
    homepage = "https://github.com/taywee/args"

    license = "MIT"

    exports = ["LICENSE.md"]
    no_copy_source = True

    def source(self):
        tools.get("https://github.com/Taywee/args/archive/{version}.tar.gz".format(version=self.version))

    def package(self):
        self.copy(pattern="LICENSE", dst="licenses", src="args-" + self.version, keep_path=False)
        self.copy("args.hxx", dst="include", src="args-" + self.version, keep_path=False)
