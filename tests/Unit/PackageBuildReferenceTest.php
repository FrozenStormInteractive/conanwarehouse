<?php

namespace Tests\Unit;

use App\Conan\PackageBuildReference;
use App\Conan\PackageVersionReference;
use Tests\TestCase;

class PackageBuildReferenceTest extends TestCase
{
    public function testLoad()
    {
        $referenceStr = "opencv/2.4.10@lasote/testing:123123123";

        $result = PackageBuildReference::load($referenceStr);

        $this->assertSame("opencv", $result->packageVersionReference()->name());
        $this->assertSame("2.4.10", $result->packageVersionReference()->version());
        $this->assertSame("lasote", $result->packageVersionReference()->user());
        $this->assertSame("testing", $result->packageVersionReference()->channel());
        $this->assertNull($result->packageVersionReference()->revision());
        $this->assertSame("123123123", $result->buildId());
        $this->assertNull($result->revision());

        // -------------------------

        $referenceStr = "opencv/2.4.10@lasote/testing#23:123123123";

        $result = PackageBuildReference::load($referenceStr);

        $this->assertSame("opencv", $result->packageVersionReference()->name());
        $this->assertSame("2.4.10", $result->packageVersionReference()->version());
        $this->assertSame("lasote", $result->packageVersionReference()->user());
        $this->assertSame("testing", $result->packageVersionReference()->channel());
        $this->assertSame("23", $result->packageVersionReference()->revision());
        $this->assertSame("123123123", $result->buildId());
        $this->assertNull($result->revision());

        // -------------------------

        $referenceStr = "opencv/2.4.10@lasote/testing:123123123#989";

        $result = PackageBuildReference::load($referenceStr);

        $this->assertSame("opencv", $result->packageVersionReference()->name());
        $this->assertSame("2.4.10", $result->packageVersionReference()->version());
        $this->assertSame("lasote", $result->packageVersionReference()->user());
        $this->assertSame("testing", $result->packageVersionReference()->channel());
        $this->assertNull($result->packageVersionReference()->revision());
        $this->assertSame("123123123", $result->buildId());
        $this->assertSame("989", $result->revision());

        // -------------------------

        $referenceStr = "opencv/2.4.10@lasote/testing#23:123123123#989";

        $result = PackageBuildReference::load($referenceStr);

        $this->assertSame("opencv", $result->packageVersionReference()->name());
        $this->assertSame("2.4.10", $result->packageVersionReference()->version());
        $this->assertSame("lasote", $result->packageVersionReference()->user());
        $this->assertSame("testing", $result->packageVersionReference()->channel());
        $this->assertSame("23", $result->packageVersionReference()->revision());
        $this->assertSame("123123123", $result->buildId());
        $this->assertSame("989", $result->revision());
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testLoadWithEmptyReferenceString()
    {
        $referenceStr = "";

        PackageBuildReference::load($referenceStr);
    }

    public function testRepresentation()
    {
        // GIVEN
        $pReference = new PackageVersionReference("opencv_lite", "2.4.10", "phil_lewis", "testing");
        $reference = new PackageBuildReference($pReference, "123123");

        // WHEN
        $result = $reference->representation();
        $str = (string) $reference;

        // THEN
        $this->assertSame("opencv_lite/2.4.10@phil_lewis/testing:123123", $result);
        $this->assertSame("opencv_lite/2.4.10@phil_lewis/testing:123123", $str);
    }

    public function testRepresentationWithRevision()
    {
        // GIVEN
        $pReference = new PackageVersionReference("opencv_lite", "2.4.10", "phil_lewis", "testing", "123");
        $reference = new PackageBuildReference($pReference, "123123", "456");

        // WHEN
        $result = $reference->representation();
        $str = (string) $reference;

        // THEN
        $expected = "opencv_lite/2.4.10@phil_lewis/testing#123:123123#456";
        $this->assertSame($expected, $result);
        $this->assertSame($expected, $str);
    }
}
