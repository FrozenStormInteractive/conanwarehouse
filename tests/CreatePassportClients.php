<?php

namespace Tests;

trait CreatePassportClients
{
    protected function createPassportPersonalAccessClient()
    {
        $this->artisan('passport:client', ['--personal' => true, '--name' => ' Personal Access Client']);
    }

    protected function createPassportPasswordGrantClient()
    {
        $this->artisan('passport:client', ['--password' => true, '--name' => ' Password Grant Client']);
    }
}
