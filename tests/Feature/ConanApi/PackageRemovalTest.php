<?php

namespace Tests\Feature\ConanApi;

use App\Models\Repository;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PackageRemovalTest extends TestCase
{
    use RefreshDatabase;

    public function testDeletePackage()
    {
        Storage::fake();

        // GIVEN
        $repository = factory(Repository::class)->create();
        $package = factory(\App\Models\Package::class)->create([
            "repository_id" => $repository->id
        ]);
        $version = factory(\App\Models\PackageVersion::class)->create([
            "package_id" => $package->id
        ]);
        $build = factory(\App\Models\PackageBuild::class)->create([
            "package_version_id" => $version->id
        ]);

        Passport::actingAs($repository->owner);

        // WHEN
        $response = $this->delete("/api/{$repository->id}/v1/conans/{$version->reference->dirRepr()}/");

        // THEN
        $response->assertOk();

        $this->assertDatabaseMissing('package_versions', [
            'id' => $version->id
        ]);
        $this->assertDatabaseMissing('package_builds', [
            'id' => $build->id
        ]);

        Storage::disk()->assertMissing($version->folderPath);
    }

    public function testDeletePackageFiles()
    {
        Storage::fake();

        // GIVEN
        $repository = factory(Repository::class)->create();
        $package = factory(\App\Models\Package::class)->create([
            "repository_id" => $repository->id
        ]);
        $version = factory(\App\Models\PackageVersion::class)->create([
            "package_id" => $package->id
        ]);

        Passport::actingAs($repository->owner);

        // WHEN
        $response = $this->postJson("/api/{$repository->id}/v1/conans/{$version->reference->dirRepr()}/remove_files", [
            "files" => [
                "conanfile.py", "LICENSE.md"
            ]
        ]);

        // THEN
        $response->assertOk();

        Storage::disk()->assertMissing($version->folderPath.'/conanfile.py');
        Storage::disk()->assertMissing($version->folderPath.'/LICENSE.md');
    }

    public function testDeletePackageFilesWithMissingFile()
    {
        Storage::fake();

        // GIVEN
        $repository = factory(Repository::class)->create();
        $package = factory(\App\Models\Package::class)->create([
            "repository_id" => $repository->id
        ]);
        $version = factory(\App\Models\PackageVersion::class)->create([
            "package_id" => $package->id
        ]);

        Passport::actingAs($repository->owner);

        // WHEN
        $response = $this->postJson("/api/{$repository->id}/v1/conans/{$version->reference->dirRepr()}/remove_files", [
            "files" => [
                "conanfile.py", "conaninfo.txt"
            ]
        ]);

        // THEN
        $response->assertNotFound();
    }

    public function testDeletePackageFilesWithBadJSON()
    {
        Storage::fake();

        // GIVEN
        $repository = factory(Repository::class)->create();
        $package = factory(\App\Models\Package::class)->create([
            "repository_id" => $repository->id
        ]);
        $version = factory(\App\Models\PackageVersion::class)->create([
            "package_id" => $package->id
        ]);

        // WHEN
        $response = $this->postJson("/api/{$repository->id}/v1/conans/{$version->reference->dirRepr()}/remove_files", [
            "fils" => [
                "conanfile.py", "conaninfo.txt"
            ]
        ]);

        // THEN
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJsonStructure([
            "message",
            "errors" => [
                "files"
            ]
        ])->assertJson([
            "errors" => [
                "files" => ["The files field is required."]
            ]
        ]);
    }
}
