<?php

namespace Tests\Feature\ConanApi;

use App\Models\Repository;
use Illuminate\Support\Facades\Storage;
use Tests\CreatePassportClients;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthTest extends TestCase
{
    use RefreshDatabase;
    use CreatePassportClients;

    public function testAuthenticate()
    {
        $this->createPassportPersonalAccessClient();

        // GIVEN
        $user = factory(\App\Models\User::class)->create();
        $repository = factory(Repository::class)->create(['owner_id' => $user->id]);

        // WHEN
        $response = $this->get("/api/{$repository->id}/v1/users/authenticate", [
            'Authorization' => 'Basic '.base64_encode($user->name.':secret')
        ]);

        // THEN
        $response->assertOk();
        $this->assertNotEmpty($response->content());
        // TODO: assert token
    }

    public function testCheckCredentials()
    {
        $this->createPassportPersonalAccessClient();

        // GIVEN
        $user = factory(\App\Models\User::class)->create();
        $repository = factory(Repository::class)->create([
            'owner_id' => $user->id
        ]);

        $response = $this->get("/api/{$repository->id}/v1/users/check_credentials", [
            'Authorization' => 'Bearer '.$user->createToken('test')->accessToken
        ]);

        $response->assertOk();
        $this->assertSame($user->name, $response->content());
    }
}
