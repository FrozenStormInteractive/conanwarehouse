<?php

namespace Tests\Feature\ConanApi;

use App\Models\Repository;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BuildRemovalTest extends TestCase
{
    use RefreshDatabase;

    public function testDeleteBuilds()
    {
        Storage::fake();

        // GIVEN
        $repository = factory(Repository::class)->create();
        $package = factory(\App\Models\Package::class)->create([
            "repository_id" => $repository->id
        ]);
        $version = factory(\App\Models\PackageVersion::class)->create([
            "package_id" => $package->id
        ]);
        $build1 = factory(\App\Models\PackageBuild::class)->create([
            "package_version_id" => $version->id
        ]);
        $build2 = factory(\App\Models\PackageBuild::class)->create([
            "package_version_id" => $version->id
        ]);

        Passport::actingAs($repository->owner);

        $response = $this->postJson(
            "/api/{$repository->id}/v1/conans/{$version->reference->dirRepr()}/packages/delete",
            [
                "package_ids" => [$build1->conan_id, $build2->conan_id]
            ]
        );

        $response->assertOk();

        Storage::disk()->assertMissing($build1->folderPath);
        Storage::disk()->assertMissing($build2->folderPath);
    }

    public function testDeleteBuildsWithBadJSON()
    {
        Storage::fake();

        // GIVEN
        $repository = factory(Repository::class)->create();
        $package = factory(\App\Models\Package::class)->create([
            "repository_id" => $repository->id
        ]);
        $version = factory(\App\Models\PackageVersion::class)->create([
            "package_id" => $package->id
        ]);
        $build1 = factory(\App\Models\PackageBuild::class)->create([
            "package_version_id" => $version->id
        ]);
        $build2 = factory(\App\Models\PackageBuild::class)->create([
            "package_version_id" => $version->id
        ]);

        Passport::actingAs($repository->owner);

        $response = $this->postJson(
            "/api/{$repository->id}/v1/conans/{$version->reference->dirRepr()}/packages/delete",
            [
                "package_s" => [$build1->conan_id, $build2->conan_id]
            ]
        );

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJsonStructure([
            "message",
            "errors" => [
                "package_ids"
            ]
        ])->assertJson([
            "errors" => [
                "package_ids" => ["The package ids field is required."]
            ]
        ]);
    }
}
