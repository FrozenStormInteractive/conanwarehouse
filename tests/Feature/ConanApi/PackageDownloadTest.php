<?php

namespace Tests\Feature\ConanApi;

use App\Models\Repository;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PackageDownloadTest extends TestCase
{
    use RefreshDatabase;

    public function testGetManifest()
    {
        Storage::fake();

        // GIVEN
        $repository = factory(Repository::class)->create();
        $package = factory(\App\Models\Package::class)->create([
            "repository_id" => $repository->id
        ]);
        $version = factory(\App\Models\PackageVersion::class)->create([
            "package_id" => $package->id
        ]);

        // WHEN
        $response = $this->get("/api/{$repository->id}/v1/conans/{$version->reference->dirRepr()}/digest");

        // THEN
        $response
            ->assertStatus(200)
            ->assertJsonStructure(['conanmanifest.txt']);
    }

    public function testGetSnapshot()
    {
        Storage::fake();

        // GIVEN
        $repository = factory(Repository::class)->create();
        $package = factory(\App\Models\Package::class)->create([
            "repository_id" => $repository->id
        ]);
        $version = factory(\App\Models\PackageVersion::class)->create([
            "package_id" => $package->id
        ]);

        // WHEN
        $response = $this->get("/api/{$repository->id}/v1/conans/{$version->reference->dirRepr()}/");

        // THEN
        $response
            ->assertStatus(200)
            ->assertExactJson([
                'conanfile.py' => '2344dfb80f1a6de9d7ef696399d4bf6b',
                'conanmanifest.txt' => 'f58fca1ad9dd5ab20806b030088ebe5d',
                'LICENSE.md' => '00a0f9ac3349b5b1d05ef0e8db710c58'
            ]);
    }

    public function testGetDownloadUrls()
    {
        Storage::fake();

        // GIVEN
        $repository = factory(Repository::class)->create();
        $package = factory(\App\Models\Package::class)->create([
            "repository_id" => $repository->id
        ]);
        $version = factory(\App\Models\PackageVersion::class)->create([
            "package_id" => $package->id
        ]);

        // WHEN
        $response = $this->get("/api/{$repository->id}/v1/conans/{$version->reference->dirRepr()}/download_urls");

        // THEN
        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'conanfile.py',
                'conanmanifest.txt',
                'LICENSE.md',
            ]);
    }

    public function testDownloadFiles()
    {
        Storage::fake();

        // GIVEN
        $repository = factory(Repository::class)->create();
        $package = factory(\App\Models\Package::class)->create([
            "repository_id" => $repository->id
        ]);
        $version = factory(\App\Models\PackageVersion::class)->create([
            "package_id" => $package->id
        ]);
        $response = $this->get("/api/{$repository->id}/v1/conans/{$version->reference->dirRepr()}/download_urls");
        $body = $response->json();


        foreach ($body as $filename => $url) {
            // WHEN
            $response = $this->get($url);
            // THEN
            $response->assertOk();
        }
    }
}
