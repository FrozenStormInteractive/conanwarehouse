<?php

namespace Tests\Feature\ConanApi;

use App\Conan\PackageVersionReference;
use App\Models\Repository;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PackageUploadTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    public function testGetUploadUrls()
    {
        // GIVEN
        $repository = factory(Repository::class)->create();

        $name = str_random(10);
        $version = $this->faker->numerify("#.#.#");
        $user = $this->faker->userName;
        $channel = 'stable';
        $reference = new PackageVersionReference($name, $version, $user, $channel);

        Passport::actingAs($repository->owner);

        // WHEN
        $response = $this->postJson("/api/{$repository->id}/v1/conans/{$reference->dirRepr()}/upload_urls", [
            'conanfile.py' => 780,
            'conanmanifest.txt' => 103,
            'LICENSE.md' =>1069
        ]);

        // THEN
        $response
            ->assertOk()
            ->assertJsonStructure([
                'conanfile.py',
                'conanmanifest.txt',
                'LICENSE.md',
            ]);

        $this->assertDatabaseHas('packages', [
            'name' => $name, 'user' => $user
        ])->assertDatabaseHas('package_versions', [
            'version_string' => $version, 'channel' => $channel
        ]);
    }

    public function testUploadFiles()
    {
        Storage::fake();

        // GIVEN
        $repository = factory(Repository::class)->create();
        $name = str_random(10);
        $version = $this->faker->numerify("#.#.#");
        $user = $this->faker->userName;
        $channel = 'stable';
        $reference = new PackageVersionReference($name, $version, $user, $channel);

        Passport::actingAs($repository->owner);

        $files = [
            'conanfile.py' => 780,
            'conanmanifest.txt' => 103,
            'LICENSE.md' =>1069
        ];

        $response = $this->postJson("/api/{$repository->id}/v1/conans/{$reference->dirRepr()}/upload_urls", $files);
        $body = $response->json();
        $packageVersion = $repository->getPackageVersion($reference);

        foreach ($body as $filename => $url) {
            // WHEN
            $response = $this->call(
                'PUT',
                $url,
                [],
                [],
                [],
                [],
                UploadedFile::fake()->image($filename, $files[$filename])
            );

            // THEN
            $response->assertOk();

            Storage::assertExists($packageVersion->recipeFolderPath."/".$filename);
        }
    }
}
