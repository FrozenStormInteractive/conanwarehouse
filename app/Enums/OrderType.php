<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class OrderType extends Enum
{
    const ASC = 'asc';
    const DESC = 'desc';
}
