<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class PermissionType extends Enum
{
    const READ_ONLY = 0;
    const READ_WRITE = 1;
    const ADMIN = 2;
}
