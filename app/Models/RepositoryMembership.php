<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class RepositoryMembership extends Pivot
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'expired_at'
    ];
}
