<?php

namespace App\Models;

use App\Conan\Paths;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * @method static PackageBuild findOrFail(string $buildId)
 */
class PackageBuild extends Model
{
    protected $fillable = ['conan_id'];

    protected $casts = [
        'options' => 'array'
    ];

    /*
     * Relationships
     */

    public function version()
    {
        return $this->belongsTo(PackageVersion::class, 'package_version_id');
    }

    /*
     * Accessors & mutators
     */

    public function getFolderPathAttribute()
    {
        return $this->version->folderPath."/".Paths::BUILDS_FOLDER."/".$this->id;
    }

    public function getFilesAttribute()
    {
        return Storage::allFiles($this->folderPath);
    }

    public function getIsLinuxBuildAttribute()
    {
        return $this->os == "Linux";
    }

    public function getIsWindowsBuildAttribute()
    {
        return $this->os == "Windows" ||  $this->os == "WindowsStore";
    }

    public function getIsAndroidBuildAttribute()
    {
        return $this->os == "Android";
    }

    public function getIsAppleBuildAttribute()
    {
        return $this->os == "iOS" ||  $this->os == "watchOS" ||  $this->os == "Macos" ||  $this->os == "tvOS";
    }

    public function getIsFreeBSDBuildAttribute()
    {
        return $this->os == "FreeBSD";
    }
}
