<?php

namespace App\Models;

use App\Models\Traits\HasAvatar;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\HasApiTokens;
use Lib\Sortable\Sortable;
use Lib\Sortable\SortableContract;

class User extends Authenticatable implements MustVerifyEmail, SortableContract
{
    use HasApiTokens, Notifiable, HasAvatar, Sortable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'location', 'website_url'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'encrypted_otp_secret'
    ];

    protected $sortable = [
        'name'
    ];

    /*
     * Relationships
     */

    /**
     * The repositories owned by users.
     */
    public function repositories()
    {
        return $this->morphMany(Repository::class, 'owner');
    }

    public function contributedRepositories()
    {
        return $this->belongsToMany(Repository::class, 'repository_memberships')
            ->using(RepositoryMembership::class);
    }

    public function identities()
    {
        return $this->hasMany(SsoIdentity::class);
    }

    public function recoveryCodes()
    {
        return $this->hasMany(OtpRecoveryCode::class);
    }

    /*
     * Accessors & mutators
     */

    /**
     * Encrypt the user's otp secret.
     *
     * @param  string  $value
     */
    public function setOtpSecretAttribute($value)
    {
        if ($value === null) {
            $this->attributes['encrypted_otp_secret'] = null;
        } else {
            $this->attributes['encrypted_otp_secret'] = encrypt($value);
        }
    }

    /**
     * Decrypt the user's otp secret.
     *
     * @param  string  $value
     * @return string
     */
    public function getOtpSecretAttribute()
    {
        if ($this->encrypted_otp_secret === null) {
            return null;
        } else {
            return decrypt($this->encrypted_otp_secret);
        }
    }

    public function getOtpEnabledAttribute()
    {
        return $this->encrypted_otp_secret !== null;
    }

    /*
     * Helpers
     */

    public function hasSsoIdentity(string $provider)
    {
        return $this->identities()->where('provider', $provider)->exists();
    }

    public function regenerateRecoveryCodes()
    {
        return DB::transaction(function () {
            $this->recoveryCodes()->delete();

            $result = [];
            $codeObjects = [];
            for ($i = 0; $i < 10; $i++) {
                $code = str_random(16);
                $result[] = $code;
                $codeObjects[] = new OtpRecoveryCode(['code' => $code]);
            }

            $this->recoveryCodes()->saveMany($codeObjects);

            return $result;
        });
    }
}
