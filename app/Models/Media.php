<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $fillable = ['path', 'disk'];

    /*
     * Accessors & mutators
     */

    public function getUrlAttribute()
    {
        return route("medias.show", $this);
    }
}
