<?php

namespace App\Models;

use App\Conan\PackageVersionReference;
use App\Conan\Paths;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * @method static PackageVersion findOrFail(int $packageVersionId)
 */
class PackageVersion extends Model
{
    protected $fillable = ['version_string', 'channel'];

    /*
     * Relationships
     */

    public function package()
    {
        return $this->belongsTo(Package::class);
    }

    public function builds()
    {
        return $this->hasMany(PackageBuild::class);
    }

    /*
     * Accessors & mutators
     */

    public function getFolderPathAttribute()
    {
        return Paths::PACKAGES_FOLDER."/".$this->id;
    }

    public function getRecipeFolderPathAttribute()
    {
        return $this->folderPath."/".Paths::RECIPE_FOLDER;
    }

    public function getReferenceAttribute()
    {
        return new PackageVersionReference(
            $this->package->name,
            $this->version_string,
            $this->package->user,
            $this->channel
        );
    }

    public function getFilesAttribute()
    {
        return Storage::allFiles($this->recipeFolderPath);
    }

    // TODO: optimization
    public function getHasWindowsBuildAttribute()
    {
        return $this->builds()->whereIn('os', ['Windows', 'WindowsStore'])->count() > 0;
    }

    public function getHasLinuxBuildAttribute()
    {
        return $this->builds()->where('os', 'Linux')->count() > 0;
    }

    public function getHasAppleBuildAttribute()
    {
        return $this->builds()->whereIn('os', ['Macos', 'iOS', 'watchOS', 'tvOS'])->count() > 0;
    }

    public function getHasAndroidBuildAttribute()
    {
        return $this->builds()->where('os', 'Android')->count() > 0;
    }

    public function getHasFreeBSDBuildAttribute()
    {
        return $this->builds()->where('os', 'FreeBSD')->count() > 0;
    }

    public function getHasArduinoBuildAttribute()
    {
        return $this->builds()->where('os', 'Arduino')->count() > 0;
    }
}
