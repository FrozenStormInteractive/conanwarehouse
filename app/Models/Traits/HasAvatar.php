<?php

namespace App\Models\Traits;

use App\Models\Media;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Storage;

trait HasAvatar
{
    public function avatar()
    {
        return $this->belongsTo(Media::class, 'avatar_id');
    }

    /*
     * Accessors & mutators
     */

    public function getAvatarUrlAttribute()
    {
        if ($this->avatar) {
            return $this->avatar->url;
        } else {
            if (method_exists($this, 'defaultAvatarUrl')) {
                return $this->defaultAvatarUrl();
            }
            return property_exists($this, 'defaultAvatarUrl') ?
                $this->defaultAvatarUrl :
                asset('images/user-avatar.jpg');
        }
    }

    public function addAvatarFromRequest(string $fieldname = 'avatar')
    {
        $file = Request::file($fieldname);
        if ($file && $file->isValid()) {
            $path = $file->store('medias/avatars', 'local');

            if ($this->avatar) {
                Storage::delete($this->avatar->path);
                $this->avatar->path = $path;
                $this->avatar->save();
            } else {
                $media = Media::create([
                    'path' => $path,
                    'disk' => 'local'
                ]);
                $this->avatar()->associate($media);
            }
        }
    }
}
