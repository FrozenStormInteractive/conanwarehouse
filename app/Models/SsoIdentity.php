<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SsoIdentity extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'provider', 'extern_uid'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
