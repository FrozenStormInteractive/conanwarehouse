<?php

namespace App\Observers;

use App\Models\PackageBuild;
use App\Models\PackageVersion;

class PackageVersionObserver
{
    /**
     * Handle the package version "created" event.
     *
     * @param  \App\Models\PackageVersion  $packageVersion
     * @return void
     */
    public function created(PackageVersion $packageVersion)
    {
        //
    }

    /**
     * Handle the package version "updated" event.
     *
     * @param  \App\Models\PackageVersion  $packageVersion
     * @return void
     */
    public function updated(PackageVersion $packageVersion)
    {
        //
    }

    /**
     * Handle the package "deleting" event.
     *
     * @param  \App\Models\PackageVersion  $packageVersion
     * @return void
     */
    public function deleting(PackageVersion $packageVersion)
    {
        $packageVersion->builds->each(function (PackageBuild $version) {
            $version->delete();
        });
    }

    /**
     * Handle the package version "deleted" event.
     *
     * @param  \App\Models\PackageVersion  $packageVersion
     * @return void
     */
    public function deleted(PackageVersion $packageVersion)
    {
        \Storage::deleteDirectory($packageVersion->folder_path);
    }

    /**
     * Handle the package version "restored" event.
     *
     * @param  \App\Models\PackageVersion  $packageVersion
     * @return void
     */
    public function restored(PackageVersion $packageVersion)
    {
        //
    }

    /**
     * Handle the package version "force deleted" event.
     *
     * @param  \App\Models\PackageVersion  $packageVersion
     * @return void
     */
    public function forceDeleted(PackageVersion $packageVersion)
    {
        //
    }
}
