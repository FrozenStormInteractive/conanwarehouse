<?php

namespace App\Observers;

use App\Models\Package;
use App\Models\PackageVersion;

class PackageObserver
{
    /**
     * Handle the package "created" event.
     *
     * @param  \App\Models\Package  $package
     * @return void
     */
    public function created(Package $package)
    {
        //
    }

    /**
     * Handle the package "updated" event.
     *
     * @param  \App\Models\Package  $package
     * @return void
     */
    public function updated(Package $package)
    {
        //
    }

    /**
     * Handle the package "deleting" event.
     *
     * @param  \App\Models\Package  $package
     * @return void
     */
    public function deleting(Package $package)
    {
        $package->versions->each(function (PackageVersion $version) {
            $version->delete();
        });
    }

    /**
     * Handle the package "deleted" event.
     *
     * @param  \App\Models\Package  $package
     * @return void
     */
    public function deleted(Package $package)
    {
        if ($package->avatar) {
            $package->avatar->delete();
        }
    }

    /**
     * Handle the package "restored" event.
     *
     * @param  \App\Models\Package  $package
     * @return void
     */
    public function restored(Package $package)
    {
        //
    }

    /**
     * Handle the package "force deleted" event.
     *
     * @param  \App\Models\Package  $package
     * @return void
     */
    public function forceDeleted(Package $package)
    {
        //
    }
}
