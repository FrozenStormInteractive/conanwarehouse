<?php

namespace App\Policies;

use App\Enums\PermissionType;
use App\Enums\VisibilityType;
use App\Models\User;
use App\Models\Repository;
use Carbon\Carbon;
use Illuminate\Auth\Access\HandlesAuthorization;

class RepositoryPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can perform read operations on the repository.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Repository  $repository
     * @return mixed
     */
    public function read(?User $user, Repository $repository)
    {
        return $repository->visibility == VisibilityType::PUBLIC ||
            ($repository->visibility == VisibilityType::PRIVATE && $user != null &&
                ($repository->owner->is($user) || $repository->members()
                        ->wherePivot('expired_at', '>=', Carbon::now())
                        ->where('id', $user->id)->exists()));
    }

    /**
     * Determine whether the user can perform write operations on the repository.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Repository  $repository
     * @return mixed
     */
    public function write(User $user, Repository $repository)
    {
        return $repository->owner->is($user) || $repository->members()
                        ->wherePivot('expired_at', '>=', Carbon::now())
                        ->whereIn('permission_type', [PermissionType::READ_WRITE, PermissionType::ADMIN])
                        ->where('id', $user->id)->exists();
    }

    /**
     * Determine whether the user can perform admin operations on the repository.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Repository  $repository
     * @return mixed
     */
    public function admin(User $user, Repository $repository)
    {
        return $repository->owner->is($user) || $repository->members()
                ->wherePivot('expired_at', '>=', Carbon::now())
                ->whereIn('permission_type', [PermissionType::ADMIN])
                ->where('id', $user->id)->exists();
    }

    /**
     * Determine whether the user can view the repository.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Repository  $repository
     * @return mixed
     */
    public function view(?User $user, Repository $repository)
    {
        return $this->read($user, $repository);
    }

    /**
     * Determine whether the user can create repositories.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true; // User must be authenticated
    }

    /**
     * Determine whether the user can update the repository.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Repository  $repository
     * @return mixed
     */
    public function update(User $user, Repository $repository)
    {
        return $this->admin($user, $repository);
    }

    /**
     * Determine whether the user can delete the repository.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Repository  $repository
     * @return mixed
     */
    public function delete(User $user, Repository $repository)
    {
        return $this->admin($user, $repository);
    }

    /**
     * Determine whether the user can restore the repository.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Repository  $repository
     * @return mixed
     */
    public function restore(User $user, Repository $repository)
    {
        return $this->admin($user, $repository);
    }

    /**
     * Determine whether the user can permanently delete the repository.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Repository  $repository
     * @return mixed
     */
    public function forceDelete(User $user, Repository $repository)
    {
        return $this->admin($user, $repository);
    }
}
