<?php

namespace App\Http\Requests\Api;

use App\Enums\OrderType;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Foundation\Http\FormRequest;

class UserSearchRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'q' => 'required|string',
            'sort' => ['nullable', new EnumValue(OrderType::class)]
        ];
    }
}
