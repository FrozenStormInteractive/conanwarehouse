<?php

namespace App\Http\Requests;

use App\Enums\VisibilityType;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Foundation\Http\FormRequest;

class PackageUpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'avatar' => 'nullable|file|image|max:100',
            'description' => 'nullable|string|max:1000',
            'website-url' => 'nullable|string|url',
            'issuetracker-url' => 'nullable|string|url',
            'vcs-url' => 'nullable|string|url',
            'visibility' => ['required', 'integer', new EnumValue(VisibilityType::class, false)],
        ];
    }
}
