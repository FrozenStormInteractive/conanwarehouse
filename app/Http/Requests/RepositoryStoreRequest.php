<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class RepositoryStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'path' => [
                'required',
                'string',
                'regex:/^[a-zA-Z0-9_\.][a-zA-Z0-9_\-]*$/u'
            ],
            'description' => 'nullable|string',
            'visibility' => 'required|string|in:public,private',
        ];
    }
}
