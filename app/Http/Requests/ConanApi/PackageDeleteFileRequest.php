<?php

namespace App\Http\Requests\ConanApi;

use Illuminate\Foundation\Http\FormRequest;

class PackageDeleteFileRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'files' => 'required|array',
            'files.*' => 'string|distinct',
        ];
    }
}
