<?php

namespace App\Http\Controllers;

use App\Http\Requests\PackageUpdateRequest;
use App\Models\Package;
use Illuminate\Http\Request;

class PackageController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Package::class, 'package');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('packages.show');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function show(Package $package)
    {
        return view('packages.show', compact('package'));
    }

    public function browseFile(Package $package)
    {
        return view('packages.files', compact('package'));
    }

    public function showVersions(Package $package)
    {
        $versions = $package->versions()->with('builds')->paginate(20);
        return view('packages.versions', compact('package', 'versions'));
    }

    public function showBuilds(Request $request, Package $package)
    {
        $builds = $package->builds()->with('version');

        if ($request->has('q')) {
        }

        $builds = $builds->paginate(20);
        return view('packages.builds', compact('package', 'builds'));
    }

    public function showBuild(Package $package, string $buildId)
    {
        $build = $package->builds()->where('conan_id', $buildId)->firstOrFail();

        return view('packages.build', compact('package', 'build'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function edit(Package $package)
    {
        return view('packages.edit', compact('package'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function update(PackageUpdateRequest $request, Package $package)
    {
        $package->description = $request->get('description', '');
        if ($package->description == null) {
            $package->description = '';
        }

        $package->addAvatarFromRequest();

        $package->website_url = $request->get('website-url');
        if ($package->website_url === null) {
            $package->website_url = '';
        }

        $package->issue_tracker_url = $request->get('issuetracker-url');
        if ($package->issue_tracker_url === null) {
            $package->issue_tracker_url = '';
        }

        $package->vcs_url = $request->get('vcs-url');
        if ($package->vcs_url === null) {
            $package->vcs_url = '';
        }

        $package->visibility = $request->get('visibility');

        $package->save();

        return redirect()->route('packages.show', $package);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function destroy(Package $package)
    {
        $package->delete();
        return redirect()->route('home');
    }
}
