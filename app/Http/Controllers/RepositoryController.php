<?php

namespace App\Http\Controllers;

use App\Enums\VisibilityType;
use App\Http\Requests\RepositoryAddMemberRequest;
use App\Http\Requests\RepositoryStoreRequest;
use App\Http\Requests\RepositoryUpdateRequest;
use App\Models\Group;
use App\Models\Repository;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RepositoryController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Repository::class, 'repo');
        $this->middleware('can:admin,repo')->only(['showMembers', 'addMembers']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $repos = Repository::where('visibility', VisibilityType::PUBLIC);

        if (Auth::check()) {
            $repos->orWhere(function (EloquentBuilder $query) {
                $query->where('visibility', VisibilityType::PRIVATE)
                    ->where(function (EloquentBuilder $query) {
                        $query->where(function (EloquentBuilder $query) {
                            $query->where('owner_id', Auth::user()->id)
                            ->where('owner_type', User::class);
                        })->orWhere(function (EloquentBuilder $query) {
                            $query->where('owner_type', Group::class); // Check membership
                        });
                    });
            });
        }

        $repos = $repos->paginate(15);

        return view('repositories.index', compact('repos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('repositories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\RepositoryStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RepositoryStoreRequest $request)
    {
        $repo = new Repository($request->input());
        $repo->visibility = $request->input('visibility') == "public" ?
            VisibilityType::PUBLIC :
            VisibilityType::PRIVATE;
        $repo->owner()->associate($request->user());

        if ($repo->description === null) {
            $repo->description = '';
        }

        $repo->save();

        return redirect()->route('repos.show.key', $repo->path);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Repository  $repo
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Repository $repo)
    {
        $packages = $repo->packages()->where('visibility', VisibilityType::PUBLIC);
        if (Auth::check() && $request->user()->can('read', $repo)) {
            $packages->orWhere('visibility', VisibilityType::PRIVATE);
        }
        $packages = $packages->paginate(12);

        return view('repositories.show', compact('repo', 'packages'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Repository  $repo
     * @return \Illuminate\Http\Response
     */
    public function showWithKey(Request $request, string $key)
    {
        $repo = Repository::where('path', $key)->firstOrFail();
        $this->authorize('view', $repo);

        return $this->show($request, $repo);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Repository  $repo
     * @return \Illuminate\Http\Response
     */
    public function edit(Repository $repo)
    {
        return view('repositories.edit', compact('repo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\RepositoryUpdateRequest  $request
     * @param  \App\Models\Repository  $repo
     * @return \Illuminate\Http\Response
     */
    public function update(RepositoryUpdateRequest $request, Repository $repo)
    {
        $repo->name = $request->get('name', '');
        if ($repo->name == null) {
            $repo->name = '';
        }

        $repo->description = $request->get('description', '');
        if ($repo->description == null) {
            $repo->description = '';
        }

        $repo->visibility = $request->get('visibility');

        $repo->addAvatarFromRequest();

        $repo->save();

        return redirect()->route('repos.show', $repo);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Repository  $repo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Repository $repo)
    {
        $repo->delete();
        return redirect()->route('home');
    }

    /*
     * Members
     */

    /**
     * @param  \App\Models\Repository  $repo
     * @return \Illuminate\Http\Response
     */
    public function showMembers(Request $request, Repository $repo)
    {
        $members = $repo->members()->sort();

        if ($request->has('search')) {
            $members->where('name', 'like', '%'.$request->input('search').'%');
        }

        $members = $members->paginate(50);

        return view('repositories.members', compact('repo', 'members'));
    }

    /**
     * @param  \App\Http\Requests\RepositoryAddMemberRequest  $request
     * @param  \App\Models\Repository  $repo
     * @return \Illuminate\Http\Response
     */
    public function addMembers(RepositoryAddMemberRequest $request, Repository $repo)
    {
        $repo->members()->attach($request->input('user_ids'), [
            'expired_at' => $request->input('expires_at') ?: null,
            'permission_type' => $request->input('permission_level')
        ]);

        return redirect()->route('repos.members.show', $repo);
    }
}
