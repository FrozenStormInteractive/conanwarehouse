<?php

namespace App\Http\Controllers\Api\Conan\V1;

use App\Conan\PackageVersionReference;
use App\Conan\Paths;
use App\Http\Controllers\Controller;
use App\Http\Requests\ConanApi\BuildDeleteRequest;
use App\Models\PackageBuild;
use App\Models\Repository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Symfony\Component\Filesystem\Filesystem;

class BuildController extends Controller
{
    public function digest(int $repositoryId, string $reference, string $conanBuildId)
    {
        $repository = Repository::findOrFail($repositoryId);

        $this->authorize('read', $repository);

        $packageVersion = $repository->getPackageVersion(PackageVersionReference::loadFromUrl($reference));
        $build = $packageVersion->builds()->where('conan_id', $conanBuildId)->firstOrFail();

        $manifestPath = $build->folderPath."/".Paths::CONAN_MANIFEST;
        if (!Storage::exists($manifestPath)) {
            abort(404, "manifest not found");
        }

        return response()->json([
            Paths::CONAN_MANIFEST => URL::signedRoute('api.conan.build.file_download', [
                'repository_id' => $repositoryId,
                'package_version_id' => $packageVersion->id,
                'build_id' => $build->id,
                'filepath' => Paths::CONAN_MANIFEST,
            ])
        ]);
    }

    public function snapshot(int $repositoryId, string $reference, string $conanBuildId)
    {
        $repository = Repository::findOrFail($repositoryId);

        $this->authorize('read', $repository);

        $packageVersion = $repository->getPackageVersion(PackageVersionReference::loadFromUrl($reference));
        $build = $packageVersion->builds()->where('conan_id', $conanBuildId)->firstOrFail();

        $ret = [];
        $fileSystem = new Filesystem();
        foreach ($build->files as $filename) {
            $basename = $fileSystem->makePathRelative(str_start($filename, "/"), str_start($build->folderPath, "/"));
            $basename = substr($basename, 0, -1);

            $ret[$basename] = Cache::rememberForever('file_hash.'.$filename, function () use ($filename) {
                return md5(Storage::get($filename));
            });
        }
        return response()->json((object) $ret);
    }

    public function downloadUrls(int $repositoryId, string $reference, string $conanBuildId)
    {
        $repository = Repository::findOrFail($repositoryId);

        $this->authorize('read', $repository);

        $packageVersion = $repository->getPackageVersion(PackageVersionReference::loadFromUrl($reference));
        $build = $packageVersion->builds()->where('conan_id', $conanBuildId)->firstOrFail();

        $ret = [];
        $fileSystem = new Filesystem();
        foreach ($build->files as $filename) {
            $basename = $fileSystem->makePathRelative(str_start($filename, "/"), str_start($build->folderPath, "/"));
            $basename = substr($basename, 0, -1);

            $ret[$basename] = URL::signedRoute('api.conan.build.file_download', [
                'repository_id' => $repositoryId,
                'package_version_id' => $packageVersion->id,
                'build_id' => $build->id,
                'filepath' => $basename,
            ]);
        }
        return response()->json((object) $ret);
    }

    public function downloadFile(int $repository, int $packageVersionId, string $buildId, string $filepath)
    {
        $packageVersion = PackageBuild::findOrFail($buildId);
        return Storage::download($packageVersion->folderPath."/".$filepath);
    }

    public function uploadUrls(Request $request, int $repositoryId, string $reference, string $conanBuildId)
    {
        $repository = Repository::findOrFail($repositoryId);

        $this->authorize('write', $repository);

        $packageVersion = $repository->getPackageVersion(PackageVersionReference::loadFromUrl($reference));
        $build = $packageVersion->builds()->firstOrCreate([
            'conan_id' => $conanBuildId,
        ]);

        $ret = [];
        foreach ($request->json() as $filename => $size) {
            $ret[$filename] = URL::signedRoute('api.conan.build.file_upload', [
                'repository_id' => $repositoryId,
                'package_version_id' => $packageVersion->id,
                'build_id' => $build->id,
                'filepath' => $filename,
            ]);
        }
        return response()->json($ret);
    }

    public function uploadFile(
        Request $request,
        int $repository,
        int $packageVersionId,
        string $buildId,
        string $filepath
    ) {
        $build = PackageBuild::findOrFail($buildId);
        Storage::put(implode("/", [$build->folderPath, $filepath]), $request->getContent(true));

        if ($filepath == Paths::CONANINFO) {
            $infos = parse_ini_string($request->getContent(), true, INI_SCANNER_TYPED);

            $build->arch = array_get($infos, 'settings.arch', '');
            $build->os = array_get($infos, 'settings.os', '');
            $build->compiler = array_get($infos, 'settings.compiler', '');
            $build->compiler_version = array_get($infos, 'settings.compiler.version', '');
            $build->options = array_get($infos, 'options', '');

            $build->save();
        }
    }

    public function delete(BuildDeleteRequest $request, int $repositoryId, string $reference)
    {
        $repository = Repository::findOrFail($repositoryId);

        $this->authorize('write', $repository);

        $packageVersion = $repository->getPackageVersion(PackageVersionReference::loadFromUrl($reference));

        $buildIds = $request->input("package_ids");

        $packageVersion
            ->builds()
            ->whereIn('conan_id', $buildIds)
            ->get()
            ->each(function (PackageBuild $build, $key) {
                $build->delete();
            });
    }
}
