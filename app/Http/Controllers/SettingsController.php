<?php

namespace App\Http\Controllers;

use App\Http\Requests\PasswordUpdateRequest;
use App\Http\Requests\ProfileUpdateRequest;
use App\Models\OtpRecoveryCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class SettingsController extends Controller
{
    public function profile(Request $request)
    {
        $user = $request->user();
        return view('settings.profile', compact('user'));
    }

    public function updateProfile(ProfileUpdateRequest $request)
    {
        $user = $request->user();
        $user->fill($request->input());
        $user->addAvatarFromRequest();
        // TODO: mark as email as unverified if changed
        $user->save();
        return back();
    }

    public function password()
    {
        return view('settings.password');
    }

    public function updatePassword(PasswordUpdateRequest $request)
    {
        $user = $request->user();

        if (Hash::check($request->get('current_password'), $user->password)) {
            $user->password = Hash::make($request->get('password'));
            $user->save();
            Auth::logout();
            return redirect()->route('login');
        }
        return back()->withErrors(['current_password' => 'You must provide a valid current password']);
    }

    public function security(Request $request)
    {
        $user = $request->user();
        return view('settings.security', compact('user'));
    }

    public function setupOtp(Request $request)
    {
        if (!is_null($request->user()->encrypted_otp_secret)) {
            abort(404);
        }

        $google2fa = app('pragmarx.google2fa');

        $secret = $google2fa->generateSecretKey();
        $image = $google2fa->getQRCodeInline(config('app.name'), $request->user()->email, $secret);

        $request->session()->put('otp_secret', $secret);

        return view('otp.setup', compact('secret', 'image'));
    }

    public function validateOtpSetup(Request $request)
    {
        $data = $request->validate([
            'pin_code' => 'required|string'
        ]);

        $google2fa = app('pragmarx.google2fa');

        $secret = $request->session()->pull('otp_secret');

        if (!$google2fa->verifyGoogle2FA($secret, $data['pin_code'])) {
            return redirect()->route('settings.otp.setup')->withErrors(['pin_code' => 'Invalid pin code']);
        }

        $user = $request->user();
        $user->otp_secret = $secret;
        $user->save();

        $codes = $request->user()->regenerateRecoveryCodes();
        $fileData = $this->generateCodesFile($codes);
        return view('otp.recovery-codes', compact('codes', 'fileData'));
    }

    private function generateCodesFile($codes)
    {
        return "data:text/plain;charset=utf-8,".implode('%0A', $codes);
    }

    public function regenerateOtpRecoveryCodes(Request $request)
    {
        $codes = $request->user()->regenerateRecoveryCodes();
        $fileData = $this->generateCodesFile($codes);
        return view('otp.recovery-codes', compact('codes', 'fileData'));
    }

    public function disableOtp(Request $request)
    {
        $user = $request->user();
        $user->otp_secret = null;
        $user->save();
        return back();
    }

    public function disconnectAccount(Request $request, string $provider)
    {
        $request->user()->identities()->where('provider', $provider)->delete();
        return back();
    }

    public function applications()
    {
        return view('settings.applications');
    }

    public function accessTokens()
    {
        return view('settings.accessTokens');
    }
}
