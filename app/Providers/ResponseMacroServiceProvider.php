<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Response;

class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Register the application's response macros.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('text', function ($value) {
            $response = Response::make($value);
            $response->header('Content-Type', 'text/plain');
            return $response;
        });
    }
}
