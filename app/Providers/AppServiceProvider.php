<?php

namespace App\Providers;

use App\Models\Media;
use App\Models\Package;
use App\Models\PackageBuild;
use App\Models\PackageVersion;
use App\Observers\MediaObserver;
use App\Observers\PackageBuildObserver;
use App\Observers\PackageObserver;
use App\Observers\PackageVersionObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Package::observe(PackageObserver::class);
        PackageVersion::observe(PackageVersionObserver::class);
        PackageBuild::observe(PackageBuildObserver::class);

        Media::observe(MediaObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('conan.settings', function ($app) {
            return new \App\Conan\Settings();
        });
    }
}
