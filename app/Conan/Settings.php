<?php

namespace App\Conan;

use Illuminate\Support\Facades\Storage;
use Symfony\Component\Yaml\Yaml;

class Settings
{
    /**
     * @var array
     */
    protected $settings;

    public function __construct()
    {
        $this->settings = Yaml::parse(Storage::get('settings.yml'));
    }

    public function osList()
    {
        return array_keys($this->settings['os']);
    }

    public function archList()
    {
        return $this->settings['arch'];
    }

    public function compilerList()
    {
        return array_keys($this->settings['compiler']);
    }
}
