<?php

namespace App\Conan;

class Paths
{
    const CONAN_MANIFEST = "conanmanifest.txt";

    const CONANFILE = 'conanfile.py';

    const CONANFILE_TXT = "conanfile.txt";

    const CONANINFO = "conaninfo.txt";

    const BUILDS_FOLDER = "builds";

    const PACKAGES_FOLDER = "packages";

    const RECIPE_FOLDER = "recipe";
}
